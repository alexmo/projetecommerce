package controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import model.GenererPDF;
import model.Produit;
import model.ProduitStock;
import model.ProduitSurCommande;
import repo.ProduitStockRepository;
import repo.ProduitSurCommandeRepository;

@RestController
@CrossOrigin
public class ProduitStockController {
	
	@Autowired
	private ProduitStockRepository produitStockRepository;
	@Autowired
	private ProduitSurCommandeRepository produitSurCommandeRepository;
	
	@RequestMapping(value = "/admin/ajoutProduitStock", method = RequestMethod.POST)
	public void addProduit(@Valid @RequestBody ProduitStock produit) {
		/*System.out.println(produit);
		ProduitStock produitS = new ProduitStock(produit);
		System.out.println(produitS);*/		
		
		produitStockRepository.save(produit);
		GenererPDF.creerProduitPdf(produit);
		try {
			GenererPDF.creerProduitTable(produit);
		} catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/admin/modifProduitStock", method = RequestMethod.PUT)
	public void update(@Valid @RequestBody ProduitStock produit) throws Exception {
		System.out.println(produit.getStock());
		if ( (produit.getStock()+"") == "") {
			ProduitSurCommande produitC = new ProduitSurCommande(produit);
			produitSurCommandeRepository.save(produitC);
		} else {
			produitStockRepository.save(produit);
		}
		
	}
	
}