package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import Loire.ecommerce.MyConstants;
import model.Mail;

@Controller
public class EmailController {
 
    @Autowired
    private JavaMailSender emailSender;
 
    @ResponseBody
    @RequestMapping("/sendSimpleEmail")
    public String sendSimpleEmail() {
    	System.out.println("Préparation à l'envoi de mail");
 
        /*// Create a Simple MailMessage.
        SimpleMailMessage message = new SimpleMailMessage();
         
        message.setTo(MyConstants.FRIEND_EMAIL);
        message.setSubject("Test Simple Email");
        message.setText("Hello, Im testing Simple Email");
 
        // Send Message!
        this.emailSender.send(message);*/
    	Mail mail = new Mail(MyConstants.FRIEND_EMAIL, "Bonjour", "Hello world",emailSender);
        
        System.out.println("Mail envoyé");
 
        return "Email Sent!";
    }
 
}
