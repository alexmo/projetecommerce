package controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import model.GenererPDF;
import model.IdUtilisateurProduit;
import model.Mail;
import model.Produit;
import model.Utilisateur;
import model.UtilisateurPanier;
import model.UtilisateurPanierAffichage;
import model.UtilisateurPanierProduit;
import repo.ProduitRepository;
import repo.UtilisateurPanierRepository;
import repo.UtilisateurRepository;

@RestController
@CrossOrigin
public class PanierController {

	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private UtilisateurPanierRepository utilisateurPanierRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private JavaMailSender emailSender;

	@RequestMapping(value = "/user/ajouterAuPanier", method = RequestMethod.POST)
	public UtilisateurPanier addToPanier(Principal currentUser, @RequestBody Produit produit) {
		long doublon;
		System.out.println(produit);

		Utilisateur utilisateur = utilisateurRepository.findByEmail(currentUser.getName());
		System.out.println(utilisateur);

		long idUtilisateur = utilisateur.getId();
		long idProduit = produit.getId();

		IdUtilisateurProduit idUtilisateurProduit = new IdUtilisateurProduit(idUtilisateur, idProduit);
		doublon = utilisateurPanierRepository.countByUtilisateurAndProduit(utilisateur, produit);
		System.out.println("doublon : "+doublon);
		if (doublon == 0) {
			UtilisateurPanier utilisateurPanier = new UtilisateurPanier(idUtilisateurProduit, utilisateur, produit);
			System.out.println("new entry");
			utilisateurPanier.setQtt(1);
			utilisateurPanierRepository.save(utilisateurPanier);
		} else if (doublon > 0) {
			UtilisateurPanier utilisateurPanier = utilisateurPanierRepository.findById(idUtilisateurProduit);
			System.out.println("qtt = " + utilisateurPanier.getQtt());
			utilisateurPanier.setQtt(utilisateurPanier.getQtt()+1);
			System.out.println("qtt = " + utilisateurPanier.getQtt());
			System.out.println("idU = "+idUtilisateur);
			System.out.println("idP = "+idProduit);
			utilisateurPanierRepository.save(utilisateurPanier);
		}
		return null;
	}


	@RequestMapping(value = "/user/retirerDuPanier", method = RequestMethod.POST)
	public UtilisateurPanier removeToPanier(Principal currentUser, @RequestBody Produit produit) {
		long doublon;
		System.out.println(produit);

		Utilisateur utilisateur = utilisateurRepository.findByEmail(currentUser.getName());
		System.out.println(utilisateur);

		long idUtilisateur = utilisateur.getId();
		long idProduit = produit.getId();


		IdUtilisateurProduit idUtilisateurProduit = new IdUtilisateurProduit(idUtilisateur, idProduit);
		doublon = utilisateurPanierRepository.countByUtilisateurAndProduit(utilisateur, produit);
		System.out.println("doublon : "+doublon);
		UtilisateurPanier utilisateurPanier = utilisateurPanierRepository.findById(idUtilisateurProduit);
		if (utilisateurPanier.getQtt() <= 1) {
			//UtilisateurPanier utilisateurPanier = new UtilisateurPanier(idUtilisateurProduit, utilisateur, produit);
			System.out.println("Aucun produit");
			utilisateurPanierRepository.delete(utilisateurPanier);;
		} else if (utilisateurPanier.getQtt() > 0) {
			//UtilisateurPanier utilisateurPanier = utilisateurPanierRepository.findById(idUtilisateurProduit);
			System.out.println("qtt = " + utilisateurPanier.getQtt());
			utilisateurPanier.setQtt(utilisateurPanier.getQtt()-1);
			System.out.println("qtt = " + utilisateurPanier.getQtt());
			System.out.println("idU = "+idUtilisateur);
			System.out.println("idP = "+idProduit);
			//			utilisateurPanierRepository.updateQtt(qtt, idUtilisateur, idProduit);
			utilisateurPanierRepository.save(utilisateurPanier);
		}
		return null;
	}


	@RequestMapping(value = "/user/validerPanier", method = RequestMethod.POST)
	public UtilisateurPanier validatePanier(Principal currentUser) {
		Utilisateur utilisateur = utilisateurRepository.findByEmail(currentUser.getName());
		System.out.println("mon user "+utilisateur);

		Long idUtilisateur = utilisateur.getId();
		System.out.println("id utilisateur "+idUtilisateur);
		System.out.println(utilisateur.getClass().getSimpleName());
		List<UtilisateurPanier> utilisateurPaniers = utilisateurPanierRepository.findAllByUtilisateur(utilisateur);
		System.out.println("Il y a "+utilisateurPaniers.size()+" produits commandées");
		try {
			GenererPDF.creerCommande(utilisateurPaniers, utilisateur);
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
		for(UtilisateurPanier utilisateurPanier : utilisateurPaniers) {
			System.out.println("Mon produit "+utilisateurPanier.getProduit()+ " - "+utilisateurPanier.getQtt());
			utilisateurPanierRepository.delete(utilisateurPanier);
		}
		Mail.commande(emailSender, "src\\main\\resources\\"+utilisateur.getNom()+utilisateur.getPrenom()+".pdf", utilisateur.getEmail());
		return null;
	}

	@RequestMapping(value = "/user/monPanier", method = RequestMethod.GET)
	public UtilisateurPanierAffichage monPanier(Principal currentUser) {
		Utilisateur utilisateur = utilisateurRepository.findByEmail(currentUser.getName());
		List<UtilisateurPanier> mesPaniers = utilisateurPanierRepository.findAllByUtilisateur(utilisateur);
		List<UtilisateurPanierProduit> mesPaniersAffichage = new ArrayList<UtilisateurPanierProduit>();
		double prixTotal = 0;

		for (UtilisateurPanier panier: mesPaniers) {
			Long idProduit = panier.getId().getIdProduit();
			Produit produit = produitRepository.findOneById(idProduit);

			int qtt = panier.getQtt();

			UtilisateurPanierProduit panierAffichage = new UtilisateurPanierProduit(produit, qtt);
			mesPaniersAffichage.add(panierAffichage);

			prixTotal+=produit.getPrix()*qtt;
			
		}

		UtilisateurPanierAffichage monPanierDeProduit = new UtilisateurPanierAffichage(mesPaniersAffichage,round(prixTotal, 2));
		return monPanierDeProduit;
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

}
