package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

import model.Mail;
import model.Utilisateur;
import model.UtilisateurAffichage;
import repo.UtilisateurRepository;

@RestController
@CrossOrigin
public class UtilisateurController {

	@Autowired
	private JavaMailSender senderMail;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@RequestMapping(value = "/all/login", method = RequestMethod.POST)
	public String login(@RequestBody Utilisateur utilisateur, HttpServletResponse response) throws Exception {

		Utilisateur BDDUtilisateur = new Utilisateur();

		BDDUtilisateur = utilisateurRepository.findByEmail(utilisateur.getEmail());
		if(BDDUtilisateur != null) {
			if(utilisateur.getEmail().equals(BDDUtilisateur.getEmail()) && BCrypt.checkpw(utilisateur.getMdp(), BDDUtilisateur.getMdp())) {
				String tokenUser = "";
				try {
					Algorithm algorithm = Algorithm.HMAC256("Loire");	    		
					tokenUser = JWT.create()
							.withClaim("email", BDDUtilisateur.getEmail())
							.sign(algorithm);
				} catch (JWTCreationException exception){
					//Invalid Signing configuration
					// Couldn't convert Claims
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
					response.sendError(500, "Erreur : JWT non généré (erreur interne au serveur)");
					return null;
				}
				String tokens = "{"+'"'+"tokenUser"+'"'+":"+'"'+tokenUser+'"'+"}";
				return tokens ;
			} 

			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.sendError(403, "Le mot de passe est incorrect");
			return null;
		
		} else if (utilisateur.getEmail().equals("admin")) {
			if (utilisateur.getMdp()!=null) {
			if (utilisateur.getMdp().equals("admin")) {
				String tokenAdmin = "";
				try {
					Algorithm algorithm = Algorithm.HMAC256("Loire");	    		
					tokenAdmin = JWT.create()
							.withClaim("email", "admin")
							.sign(algorithm);
				} catch (JWTCreationException exception){
					//Invalid Signing configuration
					// Couldn't convert Claims
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
					response.sendError(500, "Erreur : JWT non généré (erreur interne au serveur)");
					return null;
				}
				String tokens = "{"+'"'+"tokenAdmin"+'"'+":"+'"'+tokenAdmin+'"'+"}";
				return tokens;
			} else {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.sendError(403, "Le mot de passe est incorrect");
				return null;
			}
			}
			else {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				response.sendError(403, "Le mot de passe est incorrect");
				return null;
			}
		}
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.sendError(401, "Cet email ne correspond à aucun utilisateur");
		return null;
	}

	@RequestMapping(value = "/admin/utilisateurs", method = RequestMethod.GET)
	public List<UtilisateurAffichage> affichageUtilisateur() {
		List<Utilisateur> utilisateurs = utilisateurRepository.findAll();
		List<UtilisateurAffichage> utilisateurAffich = new ArrayList();

		for (Utilisateur u : utilisateurs) {
			utilisateurAffich.add(new UtilisateurAffichage(u));
		}
		return utilisateurAffich;
	}

	@RequestMapping(value = "/user/connectedUser", method = RequestMethod.GET)
	public @ResponseBody UtilisateurAffichage getCurrentUser(Principal currentUser) { //Principal pour récupérer l'utilisateur connecté         
		try {
			Utilisateur utilisateur = utilisateurRepository.findByEmail(currentUser.getName()); //getName pour récupérer l'identifiant
			return new UtilisateurAffichage(utilisateur);
		} catch (NullPointerException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Email Not Found", e);
		}
	}

	@RequestMapping(value = "/all/nouvelUtilisateur", method = RequestMethod.POST)
	public UtilisateurAffichage creation(@Valid @RequestBody Utilisateur utilisateur, BindingResult result, HttpServletResponse response) throws IOException {
		System.out.println(utilisateur);
		if (utilisateurRepository.findByEmail(utilisateur.getEmail()) != null) {
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
			response.sendError(417, "Erreur : Cet utilisateur existe déjà");
			return null;
		} else if (result.hasErrors()) {
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			response.sendError(406, "Erreur : Formulaire non valide");
			return null;
		} else {
			response.setStatus(HttpServletResponse.SC_CREATED);

			//Chiffre le mot de passe en base :
			utilisateur.setMdp(new BCryptPasswordEncoder().encode(utilisateur.getMdp()));

			try {
				Mail mail = new Mail(utilisateur.getEmail(), "Création compte", "Bonjour, votre compte a bien été créé sur le site Loire.com", senderMail);
			} catch (MailSendException exception) {
				response.setStatus(HttpServletResponse.SC_CONFLICT);
				response.sendError(409, "Erreur : Mail non envoyé, désactivez l'antivirus");
				return null;
			}
			
			utilisateurRepository.save(utilisateur);
			return new UtilisateurAffichage(utilisateur);
		}
	}

	@RequestMapping(value = "/userOrAdmin/modifUtilisateur", method = RequestMethod.PUT)
	public void update(@RequestBody Utilisateur utilisateur) throws Exception { //Pas de @Valid à cause du mot de passe qui n'est pas remis ici
		String email = utilisateur.getEmail();
		Utilisateur BDDUtilisateur = utilisateurRepository.findByEmail(email);
		String mdp = BDDUtilisateur.getMdp();
		utilisateur.setMdp(mdp);
		utilisateurRepository.save(utilisateur);
	}

	@RequestMapping(value = "/userOrAdmin/deleteUtilisateur/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id, Principal currentUser, HttpServletResponse response) throws Exception {
		if (currentUser.getName().equals("admin")) {
			utilisateurRepository.deleteById(id);	
		} else if (!currentUser.getName().equals("admin")) {
			Utilisateur currentUtilisateur = new Utilisateur();
			try {
				currentUtilisateur = utilisateurRepository.findByEmail(currentUser.getName()); //getName pour récupérer l'identifiant
			} catch (NullPointerException e) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Email Not Found", e);
			}
			if (currentUtilisateur.getId() == id) {
				utilisateurRepository.deleteById(id);
			}
		}
		else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.sendError(403, "Erreur : Vous n'êtes pas authorisé à supprimer cet utilisateur");
		}
	}

	@RequestMapping(value = "/all/forgotmdp", method = RequestMethod.POST)
	public void forgotmdp(@RequestBody Utilisateur utilisateur, HttpServletResponse response) throws Exception {
		String email = utilisateur.getEmail();
		Utilisateur BDDutilisateur = utilisateurRepository.findByEmail(email);
		if(BDDutilisateur == null) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.sendError(401, "Cet email ne correspond à aucun utilisateur");
		} else {String mdp = genratecorrectmdp();
			BDDutilisateur.setMdp(new BCryptPasswordEncoder().encode(mdp));
			utilisateurRepository.save(BDDutilisateur);
			Mail.mdpOubli(senderMail, mdp, email);
		}
	}

	/*-public String genratecorrectmdp() {
		byte[] array = new byte[7]; // length is bounded by 7
		new Random().nextBytes(array);
		String generatedString = new String(array, Charset.forName("UTF-8"));

		return generatedString;
	}*/
	
	public String genratecorrectmdp() {
		  
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    String generatedString = buffer.toString();
	 
	    System.out.println(generatedString);
	    
	    return generatedString;
	}
}
