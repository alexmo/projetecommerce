package controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.Produit;
import repo.ProduitRepository;
import repo.ProduitStockRepository;
import repo.ProduitSurCommandeRepository;

@RestController
@CrossOrigin
public class ProduitController {

	@Autowired
	private ProduitRepository produitRepository;

	@Autowired
	private ProduitStockRepository produitStockRepository;

	@RequestMapping(value = "/all/produits", method = RequestMethod.GET)
	public List<Produit> voirProduits(@RequestParam(value = "sort", required= false) String sort, @RequestParam(value = "order", required = false) String order) {
		List<Produit> produits = new ArrayList<Produit>();
		if(sort != null && order != null) {
			if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("NOM")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByNomAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("NOM")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByNomDesc();
			}else if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("PRIX")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByPrixAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("PRIX")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByPrixDesc();
			}else if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("DESC")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByDescriptionAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("DESC")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByDescriptionDesc();
			}else if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("CATEG")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByCategorieAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("CATEG")) {
				produits = (List<Produit>) produitRepository.findAllByOrderByCategorieDesc();
			}else if(order.toUpperCase().equals("ASC") && sort.toUpperCase().equals("STOCK")) {
				produits = (List<Produit>) produitStockRepository.findAllByOrderByStockAsc();
			}else if(order.toUpperCase().equals("DESC") && sort.toUpperCase().equals("STOCK")) {
				produits = (List<Produit>) produitStockRepository.findAllByOrderByStockDesc();
			}
			else {
				produits = (List<Produit>) produitRepository.findAll();
			}
		}else {
			produits = (List<Produit>) produitRepository.findAll();
		}
		return produits;
	}

	@RequestMapping(value = "/all/produits/{id}", method = RequestMethod.GET)
	public Produit afficherProduitParId(@PathVariable Long id) {
		Produit produit = produitRepository.findOneById(id);
		return produit;
	}

	@RequestMapping(value = "/all/search/{nom}", method = RequestMethod.GET)
	public Iterable<Produit> search(@PathVariable String nom) {
		String req = "%"+nom+"%";
		List<Produit> produit = produitRepository.search(req);
		return produit;
	}

	@RequestMapping(value = "/admin/supprimProduit/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Long id, HttpServletRequest request,HttpServletResponse response) throws Exception {
		produitRepository.deleteById(id);
	}


	//@Autowired
	//private ProduitSurCommandeRepository produitSurCommandeRepository;
	//@RequestMapping(value = "/produit/{nom}", method = RequestMethod.GET)
	//public Iterable<Produit> afficherProduit(@PathVariable String nom) {
	//	List<Produit> produit = produitRepository.findByNom(nom);
	//	return produit;
	//}
	//
	//@RequestMapping(value = "/sansCommande",method = RequestMethod.GET)
	//public Iterable<ProduitStock> findAllProduitStock() {
	//	List<ProduitStock> produits = produitStockRepository.findAll();
	//	return produits;
	//}
	//
	//@RequestMapping(value = "/surCommande", method = RequestMethod.GET)
	//public Iterable<ProduitSurCommande> findAllProduitSurCommande() {
	//	List<ProduitSurCommande> produits = produitSurCommandeRepository.findAll();
	//	return produits;
	//}

}
