package controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import model.GenererPDF;
import model.Produit;
import model.ProduitStock;
import model.ProduitSurCommande;
import repo.ProduitSurCommandeRepository;

@RestController
@CrossOrigin
public class ProduitSurCommandeController {
	
	@Autowired
	private ProduitSurCommandeRepository produitSurCommandeRepository;
	
	@RequestMapping(value = "/admin/ajoutProduitSurCommande", method = RequestMethod.POST)
	public void addProduit(@Valid @RequestBody ProduitSurCommande produit) {
		/*System.out.println(produit);
		ProduitSurCommande produitC = new ProduitSurCommande(produit);
		System.out.println(produitC);*/
		produitSurCommandeRepository.save(produit);
		GenererPDF.creerProduitPdf(produit);
		try {
			GenererPDF.creerProduitTable(produit);
		} catch (URISyntaxException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/admin/modifProduitSurCommande", method = RequestMethod.PUT)
	public void update(@Valid @RequestBody ProduitSurCommande produit) throws Exception {
		produitSurCommandeRepository.save(produit);
	}
	
}