package controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import model.Categorie;
import model.Produit;
import repo.CategorieRepository;

@RestController
@CrossOrigin
@RequestMapping("/categories")
public class CategorieController {

	@Autowired
	private CategorieRepository categorieRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Categorie> findAllProduits() {
		List<Categorie> categories = categorieRepository.findAll();
		return categories;
	}
	
	@RequestMapping(value = "/listeProduits/{nom}",method = RequestMethod.GET)
	public Iterable<Produit> findProduitByCategorie(@PathVariable String nom) {
		List<Produit> produits = categorieRepository.findProduitByCategorie(nom);
		return produits;
	}
	
	@RequestMapping(value = "/ajoutCategorie", method = RequestMethod.POST)
	public void addCategorie(@Valid @RequestBody Categorie categorie) {
		System.out.println(categorie);
		categorieRepository.save(categorie);
	}
	
	@RequestMapping(value = "/supprimerCategorie/{id}", method = RequestMethod.DELETE)
	public void supprimerCategorie(@PathVariable Long id) {
		System.out.println(id);
		categorieRepository.deleteById(id);
	}
	
	@RequestMapping(value = "/modifierCategorie/{nom}", method = RequestMethod.PUT)
	public void modifierCategorie(@Valid@RequestBody Categorie categorie) {
		System.out.println(categorie);
		categorieRepository.save(categorie);
	}
	
}
