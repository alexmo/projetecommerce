package repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import model.IdUtilisateurProduit;
import model.Produit;
import model.Utilisateur;
import model.UtilisateurPanier;

public interface UtilisateurPanierRepository extends JpaRepository<UtilisateurPanier, Long> {
	UtilisateurPanier findById(IdUtilisateurProduit idUtilisateurProduit);
	List<UtilisateurPanier> findAllByUtilisateur(Utilisateur utilisateur);
	//ifExist
	long countByUtilisateurAndProduit(Utilisateur utilisateur, Produit produit);
	
//	@Modifying
//	@Query("update UtilisateurPanier u set u.qtt= ?1 where u.utilisateur.id= ?2 and u.produit.id = ?3")
//	void updateQtt(int qtt, long idUtilisateur, long idProduit);
}
