package repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import model.Categorie;
import model.Produit;

public interface CategorieRepository extends JpaRepository<Categorie, Long> {
	@Query(value = "Select p from Produit p join Categorie c on c.id=p.categorie.id where c.nom = ?1")
	public List<Produit> findProduitByCategorie(String nom);
}
