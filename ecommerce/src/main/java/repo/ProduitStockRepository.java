package repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Produit;
import model.ProduitStock;

public interface ProduitStockRepository extends JpaRepository<ProduitStock, Long> {
	public List<Produit> findAllByOrderByStockAsc();
	public List<Produit> findAllByOrderByStockDesc();
}
