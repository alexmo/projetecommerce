package repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import model.Utilisateur;
import model.UtilisateurAffichage;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

	Utilisateur findByEmail(String email);

	List<Utilisateur> findAllByEmail(String email);
	List<Utilisateur> findAllById(long id);

	@Query("Update Utilisateur u set u = ?1 where id = ?2")
	void update (UtilisateurAffichage utilisateurA, long id);
	
}
