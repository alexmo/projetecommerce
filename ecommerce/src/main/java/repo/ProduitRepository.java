package repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import model.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long> {
	
	Produit findById(long id);
	Produit findOneById(long id); //findById est de type Otional<Produit> pour Spring
	List<Produit> findAll();
	
	public List<Produit> findByNom(String nom);
	
	@Query(value = "SELECT p FROM Produit p WHERE p.nom like ?1 or p.description like ?1")
	public List<Produit> search(String nom);
	
	public List<Produit> findAllByOrderByNomAsc();
	public List<Produit> findAllByOrderByPrixAsc();
	public List<Produit> findAllByOrderByDescriptionAsc();
	public List<Produit> findAllByOrderByCategorieAsc();
	
	public List<Produit> findAllByOrderByNomDesc();
	public List<Produit> findAllByOrderByPrixDesc();
	public List<Produit> findAllByOrderByDescriptionDesc();
	public List<Produit> findAllByOrderByCategorieDesc();
}
