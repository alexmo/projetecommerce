package repo;

import org.springframework.data.jpa.repository.JpaRepository;

import model.ProduitSurCommande;

public interface ProduitSurCommandeRepository extends JpaRepository<ProduitSurCommande, Long> {

}
