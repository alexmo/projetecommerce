package model;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import Loire.ecommerce.MyConstants;

public class Mail {

	private SimpleMailMessage message;

	public Mail(String destinataire, String objet, String body, JavaMailSender emailSender) {

		message = new SimpleMailMessage();
		message.setTo(destinataire);
		message.setSubject(objet);
		message.setText(body);

		// Send Message!
		emailSender.send(message);
	}

	public static void mdpOubli(JavaMailSender emailSender, String mdp, String email) {

		SimpleMailMessage message;

		message = new SimpleMailMessage();
		message.setTo(email);
		message.setSubject("Nouveau mot de passe ");
		message.setText("Voici votre nouveau mot de passe "+ mdp);

		// Send Message!
		emailSender.send(message);
	}


	public static void commande(JavaMailSender emailSender, String pathPDF, String email) {
		MimeMessage message = emailSender.createMimeMessage();

		boolean multipart = true;
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, multipart);

			helper.setTo(email);
			helper.setSubject("Commande sur Loire.com");

			helper.setText("Bonjour votre commande a bien été enregistrée");

			String path1 = pathPDF;

			// Attachment 1
			FileSystemResource file1 = new FileSystemResource(new File(path1));
			helper.addAttachment("Commande.pdf", file1);

			emailSender.send(message);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
