package model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

@SuppressWarnings("serial")
@Embeddable
public class IdUtilisateurProduit implements Serializable {

	private Long idUtilisateur;
	private Long idProduit;
	
	public IdUtilisateurProduit() {
	}

	public IdUtilisateurProduit(Long idUtilisateur, Long idProduit) {
		this.idUtilisateur = idUtilisateur;
		this.idProduit = idProduit;
	}
	
	public Long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(Long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public Long getIdProduit() {
		return idProduit;
	}

	public void setIdProduit(Long idProduit) {
		this.idProduit = idProduit;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		
		IdUtilisateurProduit other = (IdUtilisateurProduit) obj;
		return Objects.equals(idUtilisateur, other.idUtilisateur) && Objects.equals(idProduit, other.idProduit); 
	}

	@Override
	public int hashCode() {
		return Objects.hash(idUtilisateur, idProduit);
	}

	@Override
	public String toString() {
		return "IdUtilisateurProduit [idUtilisateur=" + idUtilisateur + ", idProduit=" + idProduit + "]";
	}

}
