package model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class GenererPDF {
	private static int cmd = 1;
	
	public static void creerProduitPdf(Produit produit) {
		if(produit instanceof ProduitStock) {
			System.out.println("Produit stock");
		}else if (produit instanceof ProduitSurCommande) {
			System.out.println("ProduitSurCommande");
		}else {
			System.out.println("Produit");
		}

		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("src\\main\\resources\\"+produit.getNom()+".pdf"));
			cmd++;
			document.open();
			Font font = FontFactory.getFont(FontFactory.COURIER,16,BaseColor.BLACK);
			Chunk chunk = new Chunk("Nouveau Produit : "+produit,font);
			document.add(chunk);
			document.close();
		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void creerProduitTable(Produit produit) throws URISyntaxException, IOException {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("src\\main\\resources\\"+produit.getNom()+"2.pdf"));
			document.open();
			
			PdfPTable table = new PdfPTable(7);
			addTableHeader(table);
			System.out.println("Corps du tableaux");
			addRows(table, produit);
			//addCustomRows(table);

			document.add(table);
			document.close();
		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void creerCommande(List<UtilisateurPanier> utilisateurPaniers, Utilisateur utilisateur) throws URISyntaxException, IOException {
		Document document = new Document();
		Paragraph paragraph = new Paragraph();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("src\\main\\resources\\"+utilisateur.getNom()+utilisateur.getPrenom()+".pdf"));
			document.open();
			Font font = FontFactory.getFont(FontFactory.COURIER,16,BaseColor.BLACK);
			Chunk chunk = new Chunk("Commande de "+utilisateur.getPrenom()+" "+utilisateur.getNom().toUpperCase()+" - "+new Date()+"\n");
			paragraph.add(chunk);
			PdfPTable table = new PdfPTable(4);
			addTableHeaderCommande(table);
			System.out.println("Corps du tableaux");
			double totalCmdPrix = 0;
			for(UtilisateurPanier utilisateurPanier : utilisateurPaniers) {
				addRowsCommande(table, utilisateurPanier.getProduit(), utilisateurPanier.getQtt());
				totalCmdPrix += utilisateurPanier.getProduit().getPrix()*utilisateurPanier.getQtt();
			}
			paragraph.add(table);
			Chunk totalCmd = new Chunk("\nTotal de la commande : "+round(totalCmdPrix, 2)+"€");
			paragraph.add(totalCmd);
			document.add(paragraph);
			document.close();
		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void addTableHeader(PdfPTable table) {
		Stream.of("Type Produit", "Nom", "Description", "Prix", "Stock","Catégorie", "Image")
		.forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
	}
	
	private static void addTableHeaderCommande(PdfPTable table) {
		Stream.of("Nom", "Prix", "Quantité", "Total")
		.forEach(columnTitle -> {
			PdfPCell header = new PdfPCell();
			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
			header.setBorderWidth(2);
			header.setPhrase(new Phrase(columnTitle));
			table.addCell(header);
		});
	}

	private static void addRows(PdfPTable table, Produit produit) {
		String stock = "";
		String type = "Produit Sur Commande";
		if(produit instanceof ProduitStock) {
			type = "Produit Stock";
			stock = ((ProduitStock) produit).getStock() + "";
		}
		
		System.out.println("Col - 1");
		table.addCell(type);
		System.out.println("Col - 2");
		table.addCell(produit.getNom());
		System.out.println("Col - 3");
		table.addCell(produit.getDescription());
		System.out.println("Col - 4");
		table.addCell(produit.getPrix()+"€");
		System.out.println("Col - 5");
		table.addCell(stock);
		System.out.println("Col - 6");
		if(produit.getCategorie() == null) {
			System.out.println("Pas de catégorie");
			table.addCell("");
		}else {
			System.out.println("Catégorie"+produit.getCategorie().getNom());
			table.addCell(produit.getCategorie().getNom());
		}
		System.out.println("Col - 7");
		table.addCell(produit.getImage());
	}

	
	private static void addRowsCommande(PdfPTable table, Produit produit,int qtt) {
		table.addCell(produit.getNom());
		table.addCell(produit.getPrix()+"€");
		table.addCell(qtt+"");
		double total = produit.getPrix()*qtt;
		table.addCell(round(total, 2)+"€");
	}

	@SuppressWarnings("unused")
	private static void addCustomRows(PdfPTable table) 
			throws URISyntaxException, BadElementException, IOException {

		PdfPCell horizontalAlignCell1 = new PdfPCell(new Phrase("row 2, col 1"));
		horizontalAlignCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(horizontalAlignCell1);

		PdfPCell horizontalAlignCell = new PdfPCell(new Phrase("row 2, col 2"));
		horizontalAlignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(horizontalAlignCell);

		PdfPCell verticalAlignCell = new PdfPCell(new Phrase("row 2, col 3"));
		verticalAlignCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		table.addCell(verticalAlignCell);
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
}
