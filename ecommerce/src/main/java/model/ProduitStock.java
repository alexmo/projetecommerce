package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.Min;

@SuppressWarnings("serial")
@Entity
@DiscriminatorValue("ProduitStock")
public class ProduitStock extends Produit  {
	@Min(value = 0)
	private int stock = 0;

	public ProduitStock() {
	}
	
	public ProduitStock(Produit produit) {
		this.id = produit.getId();
		this.nom = produit.getNom();
		this.prix = produit.getPrix();
		this.description = produit.getDescription();
		this.image = produit.getImage();
		this.categorie = produit.getCategorie();
		this.achats = produit.getAchats();
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "ProduitStock [stock=" + stock + "]";
	}
	
}
