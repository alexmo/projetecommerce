package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
@DiscriminatorValue("ProduitSurCommande")
public class ProduitSurCommande extends Produit {

	public ProduitSurCommande() {
	}
	
	public ProduitSurCommande(Produit produit) {
		this.id = produit.getId();
		this.nom = produit.getNom();
		this.prix = produit.getPrix();
		this.description = produit.getDescription();
		this.image = produit.getImage();
		this.categorie = produit.getCategorie();
		this.achats = produit.getAchats();
	}	
	
}
