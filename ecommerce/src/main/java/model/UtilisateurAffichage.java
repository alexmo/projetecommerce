package model;

import java.time.LocalDateTime;


public class UtilisateurAffichage {

	private long id;	
	private Civilite civilite;
	private String nom;
	private String prenom;
	private String email;
	private String telephone;
	private Adresse adresse;
	private LocalDateTime dateInscription;
	
	public UtilisateurAffichage() {
	}

	public UtilisateurAffichage(Utilisateur utilisateur) {
		this.id = utilisateur.getId();
		this.civilite = utilisateur.getCivilite();
		this.nom = utilisateur.getNom();
		this.prenom = utilisateur.getPrenom();
		this.email = utilisateur.getEmail();
		this.telephone = utilisateur.getTelephone();
		this.adresse = utilisateur.getAdresse();
		this.dateInscription = utilisateur.getDateInscription();
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public Civilite getCivilite() {
		return civilite;
	}
	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Adresse getAdresse() {
		return adresse;
	}
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public LocalDateTime getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(LocalDateTime dateInscription) {
		this.dateInscription = dateInscription;
	}

	@Override
	public String toString() {
		return "UtilisateurAffichage [id=" + id + ", civilite=" + civilite + ", nom=" + nom + ", prenom=" + prenom
				+ ", email=" + email + ", telephone=" + telephone + ", adresse=" + adresse + ", dateInscription="
				+ dateInscription + "]";
	}
	
	
}
