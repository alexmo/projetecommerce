package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@SuppressWarnings("serial")
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ProduitType")
public class Produit implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	Long id;
	@NotEmpty
	String nom;
	@Min(value = 0)
	double prix;
	@NotEmpty
	String description;
	@NotEmpty
//	@Pattern(regexp = "\\.(gif|jpg|jpeg|png)$", message = "Les extensions autorisées sont .png, .jpg, .jpeg, .gif")
	String image;
	
	@ManyToOne
	@JsonBackReference(value = "categorie")
	Categorie categorie;
	
	@OneToMany( mappedBy = "produit", cascade = CascadeType.PERSIST )
	@JsonManagedReference
	@JsonIgnore
	List<UtilisateurPanier> achats;
	
	public Produit() {
	}

	public Produit(String nom, double prix, String description, String image, Categorie categorie) {
		this.nom = nom;
		this.prix = prix;
		this.description = description;
		this.image = image;
		this.categorie = categorie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	
	public List<UtilisateurPanier> getAchats() {
		return achats;
	}
	public void setAchats(List<UtilisateurPanier> achats) {
		this.achats = achats;
	}

	@Override
	public String toString() {
		return "Produit [id=" + id + ", nom=" + nom + ", prix=" + prix + ", description=" + description + ", image="
				+ image + ", categorie=" + categorie + "]";
	}
	
}
