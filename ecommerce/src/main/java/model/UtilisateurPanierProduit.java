package model;

public class UtilisateurPanierProduit {
	
	private Produit produit;
	private int qtt;
	
	public UtilisateurPanierProduit() {
	}
	
	public UtilisateurPanierProduit(Produit produit, int qtt) {
		this.produit = produit;
		this.qtt = qtt;
	}

	public Produit getProduit() {
		return produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public int getQtt() {
		return qtt;
	}

	public void setQtt(int qtt) {
		this.qtt = qtt;
	}

	@Override
	public String toString() {
		return "UtilisateurPanierAffichage [produit=" + produit + ", qtt=" + qtt + "]";
	}
	
}
