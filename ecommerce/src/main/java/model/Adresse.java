package model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;

@SuppressWarnings("serial")
@Embeddable
public  class Adresse  implements Serializable {

	@NotEmpty
    int numero;
	@NotEmpty
    String rue;
	@NotEmpty
    String cp;
	@NotEmpty
    String commune;
	
    public Adresse() {
	}

	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getCommune() {
		return commune;
	}
	public void setCommune(String commune) {
		this.commune = commune;
	}

	@Override
	public String toString() {
		return "Adresse [numero=" + numero + ", rue=" + rue + ", cp=" + cp + ", commune=" + commune + "]";
	}
	
}