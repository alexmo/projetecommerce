package model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.fasterxml.jackson.annotation.JsonBackReference;

@SuppressWarnings("serial")
@Entity
public class UtilisateurPanier implements Serializable {
	
	@EmbeddedId
	private IdUtilisateurProduit id;
	
	@ManyToOne
	@MapsId("idUtilisateur")
	@JsonBackReference(value = "utilisateur")
	private Utilisateur utilisateur;
	
	@ManyToOne
	@MapsId("idProduit")
	@JsonBackReference(value = "produit")
	private Produit produit;
	
	int qtt;

	public UtilisateurPanier() {
	}
	
	public UtilisateurPanier(IdUtilisateurProduit id, Utilisateur utilisateur, Produit produit) {
		this.id = id;
		this.utilisateur = utilisateur;
		this.produit = produit;
	}

	public UtilisateurPanier(Utilisateur utilisateur, Produit produit, int qtt) {
		this.id = new IdUtilisateurProduit(utilisateur.getId(), produit.getId());
		this.utilisateur = utilisateur;
		this.produit = produit;
		this.qtt = qtt;
	}

	public IdUtilisateurProduit getId() {
		return id;
	}
	public void setId(IdUtilisateurProduit id) {
		this.id = id;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Produit getProduit() {
		return produit;
	}
	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public int getQtt() {
		return qtt;
	}
	public void setQtt(int qtt) {
		this.qtt = qtt;
	}

	@Override
	public String toString() {
		return "UtilisateurPanier [id=" + id + ", utilisateur=" + utilisateur + ", produit=" + produit + ", qtt=" + qtt
				+ "]";
	}
	
}
