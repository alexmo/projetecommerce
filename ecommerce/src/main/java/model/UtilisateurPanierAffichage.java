package model;

import java.util.List;

public class UtilisateurPanierAffichage {

	private List<UtilisateurPanierProduit> panier;
	private double prixTotal;
	
	public UtilisateurPanierAffichage() {
	}
	
	public UtilisateurPanierAffichage(List<UtilisateurPanierProduit> panier, double prixTotal) {
		this.panier = panier;
		this.prixTotal = prixTotal;
	}

	public List<UtilisateurPanierProduit> getPanier() {
		return panier;
	}

	public void setPanier(List<UtilisateurPanierProduit> panier) {
		this.panier = panier;
	}

	public double getPrixTotal() {
		return prixTotal;
	}

	public void setPrixTotal(double prixTotal) {
		this.prixTotal = prixTotal;
	}

	@Override
	public String toString() {
		return "UtilisateurPanierAffichageTotal [panier=" + panier + ", prixTotal=" + prixTotal + "]";
	}
}
