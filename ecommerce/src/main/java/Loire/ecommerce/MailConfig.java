package Loire.ecommerce;
import java.util.Properties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {
	
	@Bean
	public JavaMailSender getJavaMailSender() {
		JavaMailSenderImpl mailSender = new  JavaMailSenderImpl();
		
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		
		mailSender.setJavaMailProperties(this.getMailProperties());
		
		return mailSender;
	}
	
	private Properties getMailProperties() {
		Properties propos = new Properties();
		
		
		propos.put("mail.smtp.ssl.protocols", "TLSv1.2"); 
		propos.put("mail.transport.protocol", "smtp");
		propos.put("mail.smtp.auth", "true");
		propos.put("mail.smtp.starttls.enable", "true");
		propos.put("mail.debug", "true");
		propos.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		
		return propos;
	}
}