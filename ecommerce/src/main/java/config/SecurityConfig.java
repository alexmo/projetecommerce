package config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import model.Utilisateur;
import repo.UtilisateurRepository;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
    private JwtAuthorizationTokenFilter authenticationTokenFilter;	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> mem = auth.inMemoryAuthentication();
		
		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		
		List<Utilisateur> utilisateurs = (List<Utilisateur>)utilisateurRepository.findAll();
		for(Utilisateur utilisateur : utilisateurs) {
			mem.withUser(utilisateur.getEmail()).password(encoder.encode(utilisateur.getMdp())).roles("USER");
		}
		
		mem.withUser("admin").password(encoder.encode("admin")).roles("ADMIN");
	}

	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.authorizeRequests()
		.antMatchers("/all/**").permitAll()
		.antMatchers("/user/**").hasRole("USER")
		.antMatchers("/admin/**").hasRole("ADMIN")
		.anyRequest()
		.authenticated()
		.and()
		.httpBasic()
		.and()
		.cors()
		.and()
		.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
	  @Bean
	  CorsConfigurationSource corsConfigurationSource() {
		  CorsConfiguration configuration = new CorsConfiguration();
		  
		  List<String> httpMethods = new ArrayList<String>();
		  httpMethods.add("GET"); httpMethods.add("POST"); httpMethods.add("PUT"); httpMethods.add("DELETE");
		  configuration.setAllowedMethods(httpMethods);
		  
		  List<String> httpOrigins = new ArrayList<String>();
		  httpOrigins.add("*");
		  configuration.setAllowedOrigins(httpOrigins);
		  
		  List<String> httpHeaders = new ArrayList<String>();
		  httpHeaders.add("*");
		  configuration.setAllowedHeaders(httpHeaders);
		  
	      UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	      source.registerCorsConfiguration("/**", configuration);
	      return source;
	  }
	  
	
}
