package config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import model.Utilisateur;
import repo.UtilisateurRepository;

@Component
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {
	
	private String tokenHeader;
	@Autowired
	private UtilisateurRepository utilisateurRepository;

	public JwtAuthorizationTokenFilter(@Value("AUTHORIZATION") String tokenHeader) {
        this.tokenHeader = tokenHeader;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        final String requestHeader = request.getHeader(this.tokenHeader);
        
        String authToken = null;
        DecodedJWT jwt = null;
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            authToken = requestHeader.substring(7);
            try {
                Algorithm algorithm = Algorithm.HMAC256("Loire");
                JWTVerifier verifier = JWT.require(algorithm).build();
                jwt = verifier.verify(authToken);
            } catch (JWTVerificationException exception){
            	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
   			 	response.sendError(403, "Token invalide : accès refusé");
            }
            
        } else {
            System.out.println("couldn't find bearer string, will ignore the header");
        }

        if (jwt != null && SecurityContextHolder.getContext().getAuthentication() == null) {
        	String email = jwt.getClaim("email").asString();
        	String mdp = "";
        	if (!email.equals("admin")) {
        		Utilisateur utilisateur = utilisateurRepository.findByEmail(email);
        		mdp = utilisateur.getMdp();
        	}
        	else {
        		mdp = "admin";
        	}
        	
        	UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(email, mdp);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }
}
