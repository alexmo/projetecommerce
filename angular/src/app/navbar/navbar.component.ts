import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
 private nom: string;

 private navbarOpen = false;

  constructor(
    private router: Router,

    ) { }

  ngOnInit() {
  }

  isAdmin(): string {
    return sessionStorage.getItem('tokenAdmin');
  }

  isUser(): string {
    return sessionStorage.getItem('tokenUser');
  }

  logout(): void {
    sessionStorage.clear();
    alert('Vous avez bien été déconnecté');
  }

  search(): void {
    this.router.navigate(['/reloadSearch/' + this.nom]);
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

}
