import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

    // constructor(private auth: AuthService) {}

    getAuthorizationToken(): string {
        if (sessionStorage.getItem('tokenAdmin')) {
          return `Bearer ${sessionStorage.getItem('tokenAdmin')}`;
        } else if (sessionStorage.getItem('tokenUser')) {
          return `Bearer ${sessionStorage.getItem('tokenUser')}`;
        } else {
          return '';
        }
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // Get the auth token from the service.
        // const authToken = this.auth.getAuthorizationToken();
        const authToken = this.getAuthorizationToken();

        // Clone the request and replace the original headers with
        // cloned headers, updated with the authorization.
        const authReq = req.clone({
          headers: req.headers.set('Authorization', authToken)
        });

        // send cloned request with header to the next handler.
        return next.handle(authReq);
  }
}
