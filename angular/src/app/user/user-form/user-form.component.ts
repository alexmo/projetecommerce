import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { Adresse } from '../adresse';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  user: User;
  mdpVerif: string;

  constructor(
    private userService: UserService,
    private router: Router
    ) { }

  ngOnInit() {
    this.user = new User();
    this.user.adresse = new Adresse();
  }

  createUser() {
    this.userService.createUser(this.user)
      .subscribe(user =>  {
        alert(`Votre compte a été créer avec succès ${user.civilite} ${user.nom}`);
        this.router.navigate(['/connexion']);
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
  }
}
