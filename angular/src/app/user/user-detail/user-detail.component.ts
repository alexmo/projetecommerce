import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { Adresse } from '../adresse';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  @Output() isLocked = new EventEmitter<boolean>();
  @Output() isUnLocked = new EventEmitter<boolean>();

  @Input() user: User;
  modifsEnCours: boolean;
  userModif: User = new User();

  constructor(
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.modifsEnCours = false;
    if (sessionStorage.getItem('tokenUser')) {
      this.user = new User();
      this.getUser();
    }
  }

  refresh(): void {
    this.modifsEnCours = false;
    this.isUnLocked.emit(false);
    if (sessionStorage.getItem('tokenUser')) {
      this.user = new User();
      this.getUser();
    }
  }

  getUser() {
    this.userService.getUser()
      .subscribe(user =>  {
        this.user = user;
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
  }

  deleteUser(user: User): void {
    if (confirm('Etes-vous sûr de vouloir supprimer ce compte ?')) {
    this.userService.deleteUser(user.id)
      .subscribe(_ =>  {
        sessionStorage.setItem('tokenUser', '');
        if (!(sessionStorage.getItem('tokenUser')) && !(sessionStorage.getItem('tokenAdmin'))) {
          this.router.navigate(['']);
        } else {
          this.refresh();
        }
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
    }
  }

  modifyUser(user: User): void {
    this.userService.modifyUser(this.userModif)
      .subscribe(_ =>  {
        this.refresh();
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
  }

  initForm(): void {
    this.modifsEnCours = true;
    this.isLocked.emit(true);

    this.userModif.id = this.user.id;
    this.userModif.email = this.user.email;
    this.userModif.civilite = this.user.civilite;
    this.userModif.nom = this.user.nom;
    this.userModif.prenom = this.user.prenom;
    this.userModif.telephone = this.user.telephone;
    this.userModif.dateInscription = this.user.dateInscription;

    this.userModif.adresse = new Adresse();
    this.userModif.adresse.numero = this.user.adresse.numero;
    this.userModif.adresse.rue = this.user.adresse.rue;
    this.userModif.adresse.cp = this.user.adresse.cp;
    this.userModif.adresse.commune = this.user.adresse.commune;
  }
}
