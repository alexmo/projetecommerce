import { Adresse } from './adresse';
import { Panier } from '../panier/panier';

export class User {
    id: number;
    civilite: string;
    nom: string;
    prenom: string;
    email: string;
    mdp: string;
    telephone: string;
    adresse: Adresse;
    panier: Panier;
    dateInscription: Date;
}
