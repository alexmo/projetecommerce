export class Adresse {
    numero: number;
    rue: string;
    cp: string;
    commune: string;
}
