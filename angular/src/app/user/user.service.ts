import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private springUrl = 'http://localhost:8090';

  constructor(private http: HttpClient) { }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.springUrl}/all/nouvelUtilisateur`, user);
  }

  modifyUser(user: User): Observable<User> {
    return this.http.put<User>(`${this.springUrl}/userOrAdmin/modifUtilisateur`, user);
  }

  getUser(): Observable<User> {
    return this.http.get<User>(`${this.springUrl}/user/connectedUser`);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.springUrl}/admin/utilisateurs`);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete<any>(`${this.springUrl}/userOrAdmin/deleteUtilisateur/${id}`);
  }

}
