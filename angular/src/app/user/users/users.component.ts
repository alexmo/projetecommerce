import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  selectedUser: User;
  modifsEnCours: boolean;

  constructor(
    private userService: UserService,
    private router: Router
    ) { }

  ngOnInit() {
    this.getUsers();
    this.modifsEnCours = false;
  }

  getUsers(): void {
    if (sessionStorage.getItem('tokenAdmin')) {
    this.userService.getUsers()
    .subscribe(users => this.users = users, err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  } else {
    alert('Accès refusé, veuillez vous connecter en tant qu\'admin');
    this.router.navigate(['/connexion']);
  }
  }

  onSelect(user: User): void {
    if (this.modifsEnCours) {
      alert('Veuillez finir vos modifications');
    } else {
      this.selectedUser = user;
    }
  }

  isLocked(output: boolean) {
    this.modifsEnCours = output;
  }

  isUnLocked(output: boolean) {
    this.modifsEnCours = output;
    this.getUsers();
    this.selectedUser = null;
    }


}
