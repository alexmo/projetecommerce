import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product/product.service';
import { Product } from '../product/product';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

products: Product[];

  constructor(
    private productservice: ProductService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.search();
  }

  search(): void {
    const nom = this.route.snapshot.paramMap.get('nom');
    this.productservice.searchProduct(nom).subscribe (products => {
this.products = products; }, err => {
  if (err.error.message) {
    alert(err.error.message);
  } else {
    alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
  }
});
}}
