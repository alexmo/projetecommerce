import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ProductsComponent } from './product/products/products.component';
import { ProductFormComponent } from './product/product-form/product-form.component';
import { UsersComponent } from './user/users/users.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { PanierComponent } from './panier/panier.component';
import {MentionsLegalesComponent} from './mentions-legales/mentions-legales.component';
import {FaqLoireComponent} from './faq-loire/faq-loire.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { SearchComponent } from './search/search.component';
import { ReloadSearchComponent } from './reload-search/reload-search.component';

const routes: Routes = [
  { path: '', redirectTo: '/accueil', pathMatch: 'full' },
  { path: 'accueil', component: HomeComponent },
  { path: 'connexion', component: LoginFormComponent },
  { path: 'nouvelUtilisateur', component: UserFormComponent },
  { path: 'monCompte', component: UserDetailComponent },
  { path: 'monPanier', component: PanierComponent },
  { path: 'utilisateurs', component: UsersComponent },
  { path: 'nouveauProduit', component: ProductFormComponent },
  { path: 'produits', component: ProductsComponent },
  { path: 'produits/:id', component: ProductDetailComponent },
  { path: 'mentionsLegales', component: MentionsLegalesComponent },
  { path: 'faq', component: FaqLoireComponent},
  { path: 'reloadSearch/:nom', component: ReloadSearchComponent},
  { path: 'search/:nom', component: SearchComponent}

];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule { }
