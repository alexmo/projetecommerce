import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reload-search',
  templateUrl: './reload-search.component.html',
  styleUrls: ['./reload-search.component.css']
})
export class ReloadSearchComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const nom = this.route.snapshot.paramMap.get('nom');
    this.router.navigate(['/search/' + nom]);
  }

}
