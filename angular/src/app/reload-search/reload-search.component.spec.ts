import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReloadSearchComponent } from './reload-search.component';

describe('ReloadSearchComponent', () => {
  let component: ReloadSearchComponent;
  let fixture: ComponentFixture<ReloadSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReloadSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReloadSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
