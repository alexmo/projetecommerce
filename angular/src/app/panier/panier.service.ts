import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PanierTotal } from './panierTotal';
import { Product } from '../product/product';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  private springUrl = 'http://localhost:8090';

  constructor(private http: HttpClient) { }

  getPanier(): Observable<PanierTotal> {
    return this.http.get<PanierTotal>(`${this.springUrl}/user/monPanier`);
  }

  removeProduct(product: Product): Observable<any> {
    return this.http.post<any>(`${this.springUrl}/user/retirerDuPanier`, product);
  }

  addProduct(product: Product) {
    return this.http.post<Product>(`${this.springUrl}/user/ajouterAuPanier`, product);
  }

  validerPanier() {
    return this.http.post<any>(`${this.springUrl}/user/validerPanier`, null);
  }
}
