import { Component, OnInit } from '@angular/core';
import { PanierService } from './panier.service';
import { Panier } from './panier';
import { PanierTotal } from './panierTotal';
import { Product } from '../product/product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  constructor(
    private panierService: PanierService,
    private router: Router) { }

  panierTotal: PanierTotal;
  paniers: Panier[];

  ngOnInit() {
    this.getPanier();
  }

  getPanier(): void {
    this.panierService.getPanier().subscribe(panierTotal => {
      this.panierTotal = panierTotal;
      this.paniers = this.panierTotal.panier;
    }, err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  }

  removeProduct(product: Product): void {
    this.panierService.removeProduct(product).subscribe(_ => {
      this.getPanier();
    }, err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  }

  addProduct(product: Product): void {
    this.panierService.addProduct(product).subscribe(_ => {
      this.getPanier();
    }, err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  }

  validerPanier(product: Product): void {
    this.panierService.validerPanier().subscribe(_ => {
      alert('Votre commande a été validée');
      this.router.navigate(['produits']);
    }, err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  }
}
