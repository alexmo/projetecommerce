import { Panier } from './panier';

export class PanierTotal {
    panier: Panier[];
    prixTotal: number;
}