import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): void {
    this.productService.getProducts()
    .subscribe(product => this.products = product, err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  }
}
