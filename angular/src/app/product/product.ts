export class Product {
    id: number;
    nom: string;
    prix: number;
    image: string;
    description: string;
    stock: number;
    // idCategorie: number;
}
