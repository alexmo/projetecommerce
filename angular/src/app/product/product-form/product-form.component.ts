import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router} from '@angular/router';
import { Product } from '../product';
@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
product: Product;

  constructor(
  private productService: ProductService,
  private router: Router) { }

  ngOnInit() {
    this.product = new Product();
  }

  createProduit() {

//  this.ProductService.createProduit(this.product).subscribe(_ =>  {
//  alert(`Votre produit a été ajouté ' ${this.product} ${this.product.nom}`);})

    if (this.product.stock >= 0) {
      this.productService.createProduitStock(this.product).subscribe(_ =>  {
        this.router.navigate(['produits']);
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
    } else {
      this.productService.createProduitSurCommande(this.product).subscribe(_ =>  {
        this.router.navigate(['produits']);
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
    }
  }
}
