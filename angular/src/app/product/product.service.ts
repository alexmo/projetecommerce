import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private springUrl = 'http://localhost:8090';

  constructor(private http: HttpClient) { }

  createProduitSurCommande(product: Product): Observable<Product> {
    return this.http.post<Product>(`${this.springUrl}/admin/ajoutProduitSurCommande`, product);
  }

  createProduitStock(product: Product): Observable<Product> {
    return this.http.post<Product>(`${this.springUrl}/admin/ajoutProduitStock`, product);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.springUrl}/all/produits`);
  }

  getProductById(id: number): Observable<Product> {
    return this.http.get<Product>(`${this.springUrl}/all/produits/${id}`);
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete<any>(`${this.springUrl}/admin/supprimProduit/${id}`);
  }

  //  modifieProduct(productModif: Product): Observable<Product> {
    //      return this.http.put<Product>(`${this.springUrl}/admin/modifProduit`, productModif);
    //  }

    modifierProduitSurCommande(productModif: Product): Observable<Product> {
      return this.http.put<Product>(`${this.springUrl}/admin/modifProduitSurCommande`, productModif);
    }

    modifierProduitStock(productModif: Product): Observable<Product> {
      return this.http.put<Product>(`${this.springUrl}/admin/modifProduitStock`, productModif);
    }

    searchProduct(nom: string)  {
      return this.http.get<Product[]>(`${this.springUrl}/all/search/${nom}` );
    }

  }
