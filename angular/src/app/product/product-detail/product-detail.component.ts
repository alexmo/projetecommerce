import { Component, OnInit, Input} from '@angular/core';
import { Product } from '../product';
import { ProductService } from '../product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PanierService } from '../../panier/panier.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: Product;
  productModif: Product;
  valid: boolean;

  constructor(
    private productService: ProductService,
    private panierService: PanierService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
    ) { }

  ngOnInit() {
    this.getProductById();
  }

  reset(): void {
    this.valid = false;
    this.productModif = new Product();
    this.productModif.id = this.product.id;
    this.productModif.nom = this.product.nom;
    this.productModif.description = this.product.description;
    this.productModif.stock = this.product.stock;
    this.productModif.prix = this.product.prix;
    this.productModif.image = this.product.image;
  }

  getProductById(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProductById(id)
      .subscribe(product => {
        this.product = product;
        this.reset();
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }});
  }

modifieProduct() {
    if (this.productModif.stock >= 0) {
      this.productService.modifierProduitStock(this.productModif).subscribe(_ =>  {
        this.reset();
        this.getProductById();
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
    } else {
      this.productService.modifierProduitSurCommande(this.productModif).subscribe(_ =>  {
        this.reset();
        this.getProductById();
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
    }
  }

  buyProduct(product: Product): void {
    this.panierService.addProduct(product)
    .subscribe(_ => this.router.navigate(['monPanier']), err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  }

  deleteProduct(product: Product): void {
    if (confirm('Etes-vous sûr de vouloir supprimer ce produit ?')) {
    this.productService.deleteProduct(this.product.id)
      .subscribe(_ => this.router.navigate(['produits']), err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
    }
  }

  refresh(): void {
    this.valid = false;
  }

  goBack(): void {
    this.location.back();
  }

  isAdmin(): string {
    return sessionStorage.getItem('tokenAdmin');
  }

  isUser(): string {
    return sessionStorage.getItem('tokenUser');
  }

}
