import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { httpInterceptorProviders } from './http-interceptor';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './product/products/products.component';
import { UsersComponent } from './user/users/users.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { ProductFormComponent } from './product/product-form/product-form.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { PanierComponent } from './panier/panier.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FaqLoireComponent } from './faq-loire/faq-loire.component';
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { PiedPageComponent } from './pied-page/pied-page.component';
import { SearchComponent } from './search/search.component';
import { ReloadSearchComponent } from './reload-search/reload-search.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserDetailComponent,
    UserFormComponent,
    UsersComponent,
    ProductDetailComponent,
    ProductFormComponent,
    ProductsComponent,
    LoginFormComponent,
    PanierComponent,
    NavbarComponent,
    FaqLoireComponent,
    MentionsLegalesComponent,
    PiedPageComponent,
    SearchComponent,
    ReloadSearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
