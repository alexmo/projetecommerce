import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../user/user';
import { Token } from './token';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private springUrl = 'http://localhost:8090';

  constructor(private http: HttpClient) { }

  login(user: User): Observable<Token> {
    return this.http.post<Token>(`${this.springUrl}/all/login`, user);
  }

  resetMdp(user: User): Observable<any> {
    return this.http.post<any>(`${this.springUrl}/all/forgotmdp`, user);
  }
}
