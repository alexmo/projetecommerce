import { Component, OnInit } from '@angular/core';
import { User } from '../user/user';
import { AuthorizationService } from './authorization.service';
import { Token } from './token';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  user: User;

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router
    ) { }

  ngOnInit() {
    sessionStorage.clear();
    this.user = new User();
  }

  login() {
    this.authorizationService.login(this.user).subscribe((tokens: Token) => {
        if (tokens.tokenAdmin) {
            sessionStorage.setItem('tokenAdmin', tokens.tokenAdmin);
            this.router.navigate(['']);
            alert('Connexion avec succès en mode admin');
          } else if (tokens.tokenUser) {
          sessionStorage.setItem('tokenUser', tokens.tokenUser);
          this.router.navigate(['']);
          alert('Connexion avec succès');
        } else {
            alert('Echec de l\'authentification');
        }
      }, err => {
        if (err.error.message) {
          alert(err.error.message);
        } else {
          alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
        }
      });
  }

  resetMdp() {
    this.authorizationService.resetMdp(this.user).subscribe(_ => {
      alert('Un email avec votre nouveau mot de passe a été envoyé'); }
        , err => {
      if (err.error.message) {
        alert(err.error.message);
      } else {
        alert('Erreur : Veuillez vérifier que le serveur Spring est lancé');
      }
    });
  }
}
