import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaqLoireComponent } from './faq-loire.component';

describe('FaqLoireComponent', () => {
  let component: FaqLoireComponent;
  let fixture: ComponentFixture<FaqLoireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaqLoireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqLoireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
